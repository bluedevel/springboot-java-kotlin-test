package de.robinengel.springbootjavakotlintest;

import de.robinengel.springbootjavakotlintest.templates.HelloTemplateKt;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloWorldController {

    @GetMapping("/helloLegacy")
    public String helloLegacy(@RequestParam("name") String name, Model model) {
        model.addAttribute("name", name);
        return "helloWorld";
    }

    @GetMapping("/hello")
    public ResponseEntity<?> hello(@RequestParam("name") String name) {
        return HelloTemplateKt.hello(name);
    }
}
