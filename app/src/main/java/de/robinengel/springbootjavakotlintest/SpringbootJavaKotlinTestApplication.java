package de.robinengel.springbootjavakotlintest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootJavaKotlinTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJavaKotlinTestApplication.class, args);
	}

}
