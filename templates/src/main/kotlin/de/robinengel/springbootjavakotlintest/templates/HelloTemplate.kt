package de.robinengel.springbootjavakotlintest.templates

import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

fun hello(name: String): ResponseEntity<String> = ResponseEntity(buildString {
    appendln("<!DOCTYPE html>")
    appendHTML().html {
        head {
            title { +"Hello $name" }
        }
        body {
            p { +"Hello $name" }
        }
    }
}, HttpStatus.OK)